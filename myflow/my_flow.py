# flows/my_flow.py

from prefect import task, Flow
from prefect.environments.storage import Bitbucket

@task
def get_data():
    return [1, 2, 3, 4, 5]

@task
def print_data(data):
    print(data)

with Flow("file-based-flow") as flow:
    data = get_data()
    print_data(data)

flow.storage = Bitbucket(
    project="project_prefect_demo",
    repo="kingsley_prefect/project_prefect_demo.git",                 # name of repo
    path="myflow/my_flow.py",        # location of flow file in repo
    secrets=["BITBUCKET_ACCESS_TOKEN"]  # name of personal access token secret
)

# flow.register("bitbucket")
